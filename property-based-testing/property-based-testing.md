---
author: 'Anders Smedegaard Pedersen'
title: 'Property based Testing'
patat:
    theme:
        header: [vividBlue]
        other: [vividBlue]
        syntaxHighlighting:
            keyword: [rgb#FF007C]
            string: [vividYellow]
            decVal: [vividMagenta]
            float: [vividMagenta]
            constant: [vividCyan]
            variable: [rgb#FB9020]
            
    
...


```
  _____                           _            _                        _    _            _   _             
 |  __ \                         | |          | |                      | |  | |          | | (_)            
 | |__) | __ ___  _ __   ___ _ __| |_ _   _   | |__   __ _ ___  ___  __| |  | |_ ___  ___| |_ _ _ __   __ _ 
 |  ___/ '__/ _ \| '_ \ / _ \ '__| __| | | |  | '_ \ / _` / __|/ _ \/ _` |  | __/ _ \/ __| __| | '_ \ / _` |
 | |   | | | (_) | |_) |  __/ |  | |_| |_| |  | |_) | (_| \__ \  __/ (_| |  | ||  __/\__ \ |_| | | | | (_| |
 |_|   |_|  \___/| .__/ \___|_|   \__|\__, |  |_.__/ \__,_|___/\___|\__,_|   \__\___||___/\__|_|_| |_|\__, |
                 | |                   __/ |                                                           __/ |
                 |_|                  |___/                                                           |___/ 
```

> In Elixir
> TRD BEAM

---

```
#############################
#############################
##                         ##
##  ASK QUESTIONS !!! (!)  ##
##                         ##
#############################
#############################
```

---

# Agenda

* What is PBT?
* How does it work in Elixir
* Look at some code

---

# History

> QuickCheck: A Lightweight Tool for Random Testing of Haskell Programs
-- Koen Claessen and John Hughes. In Proc. of International Conference on Functional Programming (ICFP), ACM SIGPLAN, 2000.

http://users.cs.northwestern.edu/~robby/courses/395-495-2009-fall/quick.pdf

---

# In general

* Generate data
* Give data as parameters
* Assert that we get the expected behaviour

---

### Unit test
```elixir
test "can push number to list" do
  result = MyModule.push([], 47)
  assert List.first(result) == 47
end
```

##### What does this assert?

Can we push:

*   negative integers?
*   any real number?

. . .

### Property

```elixir
property "can add number to list" do
  forall {list, number} <- {list(), number()} do
    new_list = MyModule.push(list, number)
    number == List.first(new_list)
  end
end
```

---

### "I've used fuzz testing for years!"

**fuzzing**
> generating garbage input to see if the program fails...

**property-based testing**
> where we check that the program behaves the right way given all kinds of inputs.
--Fred Herbert

---

# Why Bother?

- Klarna's database crashed every month or so...
> "Race conditions and corruption of file under very specific circumstances" 

- Google's LevelDB failed
> "When doing 16 specific operations in succession"

- Testing AUTOSAR for VOLVO
> "Turning the volume up on a speccific radio makes the brake inresposive(!)"

- Finding edge cases in Dropbox

^ Watch John Hughes talks on youtube!

---

## Generators

> Generates data for your tests

```elixir
integer()
atom()
list()

## etc
```

. . .

### Combining generators

```elixir
list(integer())
union(integer(), float()) == number()
```

. . .

### Constraints

"Only generate this subset"


. . .

```elixir
non_empty(list(integer()))

integer() |> list() |> non_empty()

def non_empty_list_of_int() do
  integer() |> list() |> non_empty()
end

#### Discard genetated values if the when clause is not true

def larger_than_zero() do
  such_that n <- number(), when: n > 0
end
```

---

### Custom Types

```elixir
def norse_god() do
  oneof([:odin, :thor, :frey])
end

. . .

def isbn() do
  let isbn <- [
      oneof(['978', '979']),
      let(x <- range(0, 9999), do: to_charlist(x)),
      let(x <- range(0, 9999), do: to_charlist(x)),
      let(x <- range(0, 999), do: to_charlist(x)),
      frequency([{10, [range(?0, ?9)]}, {1, 'X'}])
    ] do
    to_string(Enum.join(isbn, "-"))
  end
end
```

. . .

#### Frequency

```elixir
frequency([
  {1, atom()},
  {10, char()},
  {3, binary()}
])
=> 23 

# (chances of getting an atom are 1 / 14)
```

---

## Shrinking (Stateless)

> _the smallest term that still fails the property_

`my_function/1` fails if the first element of the argument is `0` 

```elixir
> MyModule.my_function([SOME_LIST])

# Some very large list
[0, 2, 44, -12, 0.8721376, -1, 324, ...]

## shrink 
[0]
```

---

## Shrinking (Stateless)
#### Propcheck example

```
  1) property division is applied on the two first elements (PbtTest)
     test/pbt_test.exs:37
     Property Elixir.PbtTest.property division is applied on the two first elements() failed. Counter-Example is:
     [{[2, 0, 7, -35, -9], :/}]
```

---

# Patterns

## Modeling (Oracle Code)

> _Use code that is know to work to validate your code_

```elixir
def model_biggest(list) do
  list
  |> Enum.sort
  |> List.last
end

### 

property "finds biggest element" do
  forall x <- non_empty(list(integer())) do
    My_module.biggest(x) == model_biggest(x)
  end
end
```
---

## Invariants

> * Not varying; constant.
> * Mathematics Unaffected by a designated operation, as a transformation of coordinates.

```elixir
property "a sorted list has ordered pairs" do
  forall list <- list(term()) do
    is_ordered(Enum.sort(list))
  end
end

def is_ordered([a, b | t]) do
  a <= b and is_ordered([b | t])
end

# lists with fewer than 2 elements
def is_ordered(_) do
  true
end
```

---

## Symetric Functions 

```elixir
l = [1,2,3]

k =
  l
  |> Enum.reverse
  |> Enum.reverse

k == l
```

---

# Stateful Properties

* A Model
  Represents what the system should do at a high level
   
* A Generator for commands
  Represents the execution flow of the program
  
* An actual system
  Which is validated against our model
  
---
  
## The Model

* Represents a simple version of the actual system

### Consists of two parts

1. A data structure - Represents the state of the actual system
2. A function - That transforms the model's state. `next_state\3`, which takes `state :: dynamic_state`, `call :: symb_call` and `result :: term`

---
#### Example - File uploading service

```elixir
# A file uploading service

# Model
# map of stuff to be uploaded
%{ name => contents }

# `next_state` function
def next_state( state_map, {name, contents} = new_file, {:call, _module, :upload_file, [name, contents] = _arguments} ) do
  Map.put(state_map, name, contents)
end

```
---

## The Commands

* Represents operations that can run against the actual system


* Can be generated by the framework


* Symbolic calls


* Can be constrained with _preconditions_

---

> Tuples the represent function calls

```elixir
# general form

{ :call, Module, function, [arg1, arg2, ..., arg_N] }

# file upload example
{ :call, actual_system, :upload_file, [name, contents] }
 ```

---

## The Validation

* Check the results from the actual system match what we are expecting from our model 
* through _postconditions_
    * Either global invariants, or system output



---

## How Stateful Properties Run

- Abstract phase

    * Generate create test senario
    * Run without the actual system
    
- Actual phase

    * Also run commands against real system
    * Also checks postconditions

---

### Abstract phase

> “Its whole objective is to take the model and command generation callbacks,
> and call these to **build out the sequence of calls that will later be applied to the system.**”

---

### Abstract phase
> If precondition is false after the command is apllied, discard command

```
              if false
           ------------|
           ▼           |
init -> Command -> Precondition -> next_state
           ▲                          |
           ---------------------------|
```

> Note the lack of postcondition or calls to the actual system.

---

### Actual phase

- PropEr now (also) applies the commands to the real system
- Evaluate preconditions
- Execute symbolic calls
- Store result
- Evaluate postcondition
- State transition
- Next...

---

### Actual phase

```
init --► Command --► Precondition -- if false --► FAIL 
           ▲             |                         ▲  ▲
           |             ▼                         |  |
           |            call -- on error --------- |  |
           |             |                            |
           |             |-► postcondition -----------|  next_state
           |                             |               ▲     |
           |                             |---------------|     |
           |                                                   |
           |---------------------------------------------------|
```

---

# Shrinking (Stateful)

> "Removing operations and seeing if things still works" 

---

# The lay of the land

* [StreamData](https://hexdocs.pm/stream_data/ExUnitProperties.html)
* [Propcheck](https://hexdocs.pm/propcheck/)

---

# Sources

* [Property-Based Testing with PropEr, Erlang, and Elixir](https://pragprog.com/book/fhproper/property-based-testing-with-proper-erlang-and-elixir)
* "The Internet"

---

# Let's look at some code!

---

```elixir
def thanks_for_listening() do
  [ "Takk",
    "Thank you",
    "ありがとう",
    "Ngiyabonga",
    "🍻"]
end
```
