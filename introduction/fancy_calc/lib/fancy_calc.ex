defmodule FancyCalc do
  @moduledoc """
  Documentation for FancyCalc.
  """

  def add(n, m) when is_integer(n) and is_integer(m) do
    n + m
  end

  def add(_not_int, _also_not_int) do
    {:error, "Integers only!!!"}
  end

  def list_add(list, n) when is_integer(n) do
    [n | list]
  end

  def list_add([n, m | rest], :plus) do
    [n + m | rest]
  end
end
