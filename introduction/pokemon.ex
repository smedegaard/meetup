defmodule PokemonProgram do
  def choose_pokemon(:pikachu) do
    IO.puts("Pikachu, it’s your turn!")
  end

  def choose_pokemon(pokemon_name) do
    IO.puts("#{pokemon_name}, I choose you")
  end

  def use_more_pokemons([]) do
    IO.puts("No more Pokemon to choose!")
  end

  def use_more_pokemons(list_of_pokemon) do
    [first | rest] = list_of_pokemon
    choose_pokemon(first)
    use_more_pokemons(rest)
  end
end
